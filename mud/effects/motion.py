
from .effect import Effect2
from mud.events import MotionEvent



class MotionEffect(Effect2):
    EVENT = MotionEvent
    
    def resolve_actor(self):
        return self.resolve("this")

    def resolve_object(self):
        return self.resolve("to")

