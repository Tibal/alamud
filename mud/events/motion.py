
from .event import Event2

class MotionEvent(Event2):
    NAME = "move-object"

    @property
    def exit(self):
        return self.object

    def perform(self):
        self.actor.move_to(self.object)
